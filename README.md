# Image Colourization  

### Dataset
- We use [CelebA](http://mmlab.ie.cuhk.edu.hk/projects/CelebA.html) and [Places365](http://places2.csail.mit.edu) datasets. 

### Training
- To train the model on places365 dataset 

python train.py --seed 100 --dataset places365 --dataset-path ./dataset/places365 --checkpoints-path ./checkpoints --batch-size 16 --epochs 10 --lr 3e-4 --label-smoothing 1
```

### Evaluate

Coming Up