import glob2
import numpy as np
import tensorflow as tf
from scipy.misc import imread
from abc import abstractmethod
from .utils import unpickle

celeba_DATASET = 'celeba'
PLACES365_DATASET = 'places365'


class BaseDataset():
    def __init__(self, name, path, training=True, augment=True):
        self.name = name
        self.augment = augment and training
        self.training = training
        self.path = path
        self._data = []
        self._labels = []

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        total = len(self)
        start = 0

        while start < total:
            item = self[start]
            start += 1
            yield item

        raise StopIteration

    def __getitem__(self, index):
        val = self.data[index]
        try:
            img = imread(val) if isinstance(val, str) else val

            if self.augment and np.random.binomial(1, 0.5) == 1:
                img = img[:, ::-1, :]

        except:
            img = None

        return img

    def generator(self, batch_size, recusrive=False):
        start = 0
        # print(self)
        total = len(self)
        # print('Check',start,total)
        while True:
            while start < total:
                end = np.min([start + batch_size, total])
                items = []
                print(end)
                for ix in range(start, end):
                    item = self[ix]
                    if item is not None:
                        items.append(item)

                start = end
                yield np.array(items)

            if recusrive:
                start = 0

            else:
                raise StopIteration


    @property
    def data(self):
        if len(self._data) == 0:
            self._data, self._labels = self.load()

        return self._data, self._labels

    @abstractmethod
    def load(self):
        return []

class celebaDataset(BaseDataset):
    def __init__(self, path, training=True, augment=True):
        super(celebaDataset, self).__init__(celeba_DATASET, path, training, augment)

    def load(self):
        data = []
        if self.training:
            for i in range(1, 6):
                filename = '{}/data_batch_{}'.format(self.path, i)
                batch_data = unpickle(filename)
                if len(data) > 0:
                    data = np.vstack((data, batch_data[b'data']))
                else:
                    data = batch_data[b'data']

        else:
            filename = '{}/test_batch'.format(self.path)
            batch_data = unpickle(filename)
            data = batch_data[b'data']

        w = 32
        h = 32
        s = w * h
        data = np.array(data)
        data = np.dstack((data[:, :s], data[:, s:2 * s], data[:, 2 * s:]))
        data = data.reshape((-1, w, h, 3))
        return data



class Places365Dataset(BaseDataset):
    def __init__(self, path, training=True, augment=True):
        super(Places365Dataset, self).__init__(PLACES365_DATASET, path, training, augment)

    def load(self):
        if self.training:
            data = natsorted(np.array(
                glob2.glob(self.path + '/train/**/*.jpg', recursive=True)))
            file = open(self.path + 'train_labels.txt')
            labels = np.array(file.read().splitlines()).astype(np.uint32)
            n_values = np.max(labels) + 1
            label_one_hot = np.eye(n_values)[labels]
            data_comb = zip(data, label_one_hot)
            random.shuffle(data_comb)
            data, labels = zip(*data_comb)

        else:
            data = np.array(glob2.glob(self.path + '/val/**/*.jpg',recursive=True))
            data = data[:32]
        return data, labels
