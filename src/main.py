import os
import random
import numpy as np
import tensorflow as tf
from .options import ModelOptions
from .models import celebaModel, Places365Model
from .dataset import celeba_DATASET, PLACES365_DATASET


def main(options):

    tf.reset_default_graph()
    tf.set_random_seed(options.seed)
    np.random.seed(options.seed)
    random.seed(options.seed)


    with tf.Session() as sess:

        if options.dataset == celeba_DATASET:
            model = celebaModel(sess, options)

        elif options.dataset == PLACES365_DATASET:
            model = Places365Model(sess, options)

        if not os.path.exists(options.checkpoints_path):
            os.makedirs(options.checkpoints_path)

        if options.log:
            open(model.train_log_file, 'w').close()
            open(model.test_log_file, 'w').close()

        model.build()
        sess.run(tf.global_variables_initializer())


        model.load()


        if options.mode == 0:
            args = vars(options)
            with open(os.path.join(options.checkpoints_path, 'options.dat'), 'w') as f:
                for k, v in sorted(args.items()):
                    print('%s: %s' % (str(k), str(v)))
                    f.write('%s: %s\n' % (str(k), str(v)))
            
            model.train()

        elif options.mode == 1:
            model.evaluate()
            while True:
                model.sample()

        else:
            model.turing_test()


if __name__ == "__main__":
    main(ModelOptions().parse())
