import vgg_custom
import tensorflow as tf
import numpy as np

from sys import stderr
import time
from scipy import misc
from PIL import Image

CONTENT_LAYER = ('relu2_2','relu4_4')
network = 'imagenet-vgg-verydeep-19.mat'
pooling = 'max'
vgg_weights,vgg_mean_pixel = vgg_custom.load_net(network)

content = misc.imread('aerial_input.png')
shape = (1,) + content.shape
content_features = {}

g = tf.Graph()


with g.as_default(),tf.Session() as sess:
	image = tf.placeholder('float',shape)
	net = vgg_custom.net_preloaded(vgg_weights,image,pooling)
	content_pre = np.array([vgg_custom.preprocess(content,vgg_mean_pixel)])
	for layer in CONTENT_LAYER:
		content_features[layer] = net[layer].eval(feed_dict={image:content_pre})