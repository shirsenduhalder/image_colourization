import vgg_custom
import tensorflow as tf
import numpy as np

from sys import stderr
import time
from scipy import misc
from PIL import Image
import vgg_custom

CONTENT_LAYER = ('conv1_1','conv1_2')
VGG_MEAN = np.array([123.68,116.79,103.939])
network = 'models/vgg/imagenet-vgg-verydeep-19.mat'
pooling = 'max'
vgg_weights,vgg_mean_pixel = vgg_custom.load_net(network)

content = misc.imread('aerial_input.png')/255.0
content = misc.imresize(content,(256,256))
shape = (None,) + content.shape
content_features = {}

g = tf.Graph()


with g.as_default(),tf.Session() as sess:
	image = tf.placeholder('float',shape)
	net = vgg_custom.net_preloaded(vgg_weights,image,pooling)
	content_pre = np.array([vgg_custom.preprocess(content,vgg_mean_pixel)])
	content_pre = np.tile(content_pre,(2,1,1,1))
	for layer in CONTENT_LAYER:
		content_features[layer] = net[layer].eval(feed_dict={image:content_pre})